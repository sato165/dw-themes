<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

    <div class="container">

        <nav role="navigation">
            <?php wp_nav_menu(array(
    					'container' => 'div',                           // enter '' to remove nav container (just make sure .footer-links in _base.scss isn't wrapping)
    					'container_class' => 'footer-links cf',         // class of container (should you choose to use it)
    					'menu' => __( 'Footer Links', 'bonestheme' ),   // nav name
    					'menu_class' => 'nav footer-nav cf',            // adding custom nav class
    					'theme_location' => 'footer-links',             // where it's located in the theme
    					'before' => '',                                 // before the menu
    					'after' => '',                                  // after the menu
    					'link_before' => '',                            // before each link
    					'link_after' => '',                             // after each link
    					'depth' => 0,                                   // limit the depth of the nav
    					'fallback_cb' => 'bones_footer_links_fallback'  // fallback function
						)); ?>
        </nav>

        <p class="source-org copyright">&copy;
            <?php echo date('Y'); ?>
                <?php bloginfo( 'name' ); ?>.</p>

    </div>

</footer>
<p id="pageTop"><a href="#"><i class="fa fa-chevron-up"></i></a></p>


<?php // all js scripts are loaded in library/bones.php ?>
    <?php wp_footer(); ?>

        <script src="http://bulma.io/javascript/jquery-2.2.0.min.js"></script>
        <script src="http://bulma.io/javascript/clipboard.min.js"></script>
        <script src="http://bulma.io/javascript/bulma.js"></script>
        <script type="text/javascript">
            (function ($) {
                $(function () {
                    $(window).on('scroll', function () {

                        if ($(window).scrollTop() > 150) {
                            $('#header').addClass('fixed');
                        } else {
                            $('#header').removeClass('fixed');
                        }

                    });
                    $('#nav-toggle').click(function () {
                        $('#header').toggleClass('open');
                    });
                    $('.nav-menu > li:has( > ul)').addClass('menu-dropdown-icon');
                    $('.nav-menu > li > ul:not(:has(ul))').addClass('normal-sub');
                    $(".nav-menu li").hover(function (e) {
                        if ($(window).width() > 768) {
                            $(this).children("ul").stop(true, false).fadeToggle(150);
                            e.preventDefault();
                        }
                    });
                    $(".nav-menu > li").click(function () {
                        if ($(window).width() <= 768) {
                            $(this).children("ul").fadeToggle(150);
                            $('#nav-menu').toggleClass('open');

                        }
                    });

                    var topBtn = $('#pageTop');
                    topBtn.hide();
                    //スクロールが100に達したらボタン表示
                    $(window).scroll(function () {
                        if ($(this).scrollTop() > 100) {
                            topBtn.fadeIn();
                        } else {
                            topBtn.fadeOut();
                        }
                    });
                    //スクロールしてトップ
                    topBtn.click(function () {
                        $('body,html').animate({
                            scrollTop: 0
                        }, 500);
                        return false;
                    });


                    var w = $(window).width();
                    if (w < 480) {
                        $(window).load(function () {
                            $('.img_reprace').each(function () {
                                var img_rep_w = $(this).css("background-image");
                                img_rep_w = img_rep_w.substring(4, img_rep_w.length - 1);
                                var img = new Image();
                                img.src = img_rep_w.match(/images\/[0-9a-z]+.+[^"]/i);
                                $(this).css({
                                    height: w / img.width * img.height
                                });
                            });
                        });
                    }
                });
            })(jQuery);
        </script>

        </body>

        </html>